import board
import json

i2c = board.I2C()

while not i2c.try_lock():
    pass

id=[]
try:
 for device_address in i2c.scan():
  #print([hex(device_address)])
  id.append(hex(device_address))
finally:  # unlock the i2c bus when ctrl-c'ing out of the loop
    i2c.unlock()

with open("../server/data/dummyI2cDevices.json", "w") as outfile:
    json.dump(id, outfile)
