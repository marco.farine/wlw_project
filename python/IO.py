
import sys
import RPi.GPIO as GPIO

#call this function with 3 arguments:
#1: pinnumber
#2: mode(output = 0, input = 1)
#3: output(high = 1, low = 0) input(none = 20, pullUp = 21, pullDown =22)

GPIO.setwarnings(False)

pin = int(sys.argv[1])
mode = int(sys.argv[2])
state = int(sys.argv[3])

GPIO.setmode(GPIO.BCM)
if(mode == 0):
 GPIO.setup(pin, mode)
 GPIO.output(pin, state)
else:
 GPIO.setup(pin, mode, state)
 print(GPIO.input(pin))
