#! /usr/bin/python3

import smbus
import json
import datetime
import time
from collections import deque


class TemperatureSensor(object):
 def __init__(self):
   self.bus = smbus.SMBus(1)
 def GetTemperature(self):
  temp =  self.bus.read_byte_data(0x48,0x22)/32*0.125 + self.bus.read_byte_data(0x48,0x00)
  return temp

maxValues = 200

with open('../server/data/tempData.json', 'r') as openfile:
 json_object = json.load(openfile)
openfile.close

now = datetime.datetime.now()
now = (time.mktime(now.timetuple())*1000)
ts = TemperatureSensor()
temperature = ts.GetTemperature()
data = {"time":now,"temp":temperature}
if(len(json_object) > maxValues):
 items = deque(json_object)
 items.rotate(-1)
 json_object = list(items)
 json_object[len(json_object)-1] = data
else:
 json_object.append(data)
#print(json_object)
dump = json.dumps(json_object, indent=len(json_object))
with open('../server/data/tempData.json', 'w') as outfile:
 outfile.write(dump)
