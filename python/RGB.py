#! /usr/bin/python3


# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import board
import neopixel
import sys
import csv

rgb =[]
with open('../server/data/ledData.json') as csvfile:
 try:
  spamreader = csv.reader(csvfile, delimiter=':', quotechar='|')
  for row in spamreader:
   rgb.append(':'.join(row))
 finally:
    csvfile.close()
values =[0, 0, 0]
rgb = rgb[0].split(':')
for i in range(3):
 if(rgb[i+1][0] == 't'):
  values[i] = 255
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 16

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)
pixels.fill((values[0], values[1], values[2]))
#pixels.fill((int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3])))
pixels.show()
