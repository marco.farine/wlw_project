# wlw Projekt Möri / Farine
## Abstract
Dieses Projekt wurde im Ramen des Moduls "Workshop Linux und Webtechnologien" an der Fachhochschule Nordwerstschweiz durchgeführt. 
Es beinhaltet eine einfache Webapplikation (Server und Clientseitig) welche ein paar Beispiel-IoT Daten darstellt.
Die Applikation kann auf einem Raspberry Pi ausgeführt werden und zeigt so z.B. Temperaturdaten an.

## Getting Started
Um den Server zu starten, müssen zuerst alle nötigen Packete installiert werden:
```shell
yarn --cwd client install && \
yarn --cwd server install
```
Anschliessend kann der Server gestartet werden:
```shell
cd server && \
(sudo npm run &)
```
Sobald der Server läuft wird unter http://192.168.1.165:80 im gleien Netzwerk die Webseite angezeigt
Die Scripts für die eigentlichen Funktion müssen noch gestartet werden:
```shell
cd .. && \
cd python && \
sh pythonProcedure.sh
```

## Funktionsweise
Die folgende Abbildung zeigt die grundsätzliche Funktionsweise der Applikation.
Das Raspberry Pi liest die Daten vom Temperatursensor über I2C ein bzw. steuert das RGB LED über SPI.
Von aussen ist das Raspberry über den Express Web Server sichtbar. 
Sobald der Client einen Request an den WebServer sendet, antwortet dieser mit der Single Page Application.
Die Single Page Application besteht aus einem HTML, einem JavaScript, einem CSS File und eventuell nötiger Assets.
Die Daten, welche auf der Webseite angezeigt werden, also z.B. Temperaturdaten werden anschliessend via einzelnen API Requests beim Server abgefragt. 
![](pictures/Funktionsweise_wlw_Projekt.png)

## Hardware
Um die einzelnen Funktionen zu gewährleisten wird ein SA56004X-Temperatursensor an die I2C-Schnittstelle 1 des Raspberry Pi verbunden, GPIO2 für SDA und GPIO3 für SCL. Für die RGB-Leds wird ein Ring aus 16 WS2812B-LEDs verwendet, dabei ist die Datenlinie an GPIO18 verbunden um den PWM-Ausgang zu verwenden. Um diesen zu verwenden muss zuerst der Audioausgang deaktiviert werden. Dazu wird in der Shell mit folgenden Input das File geöffnet:
```shell
sudo nano /etc/modprobe.d/snd-blacklist.conf
```
In diesem File wird folgende Linie ergänzt:
```shell
blacklist snd_bcm2835
```
Dann muss noch die Konfigurationsdatei angepasst werden, die mit folgende Input geöffnet wird:
```shell
sudo nano /boot/config.txt
```
Und dann muss folgende Linie auskommentiert werden:
```shell
dtparam=audio=on
```
Als letztes wird noch die benötigte Bibliothek installiert mit:
```shell
sudo pip3 install adafruit-circuitpython-neopixel
```
Nach einem Neustart des Raspberry Pi ist dieser nun bereit die Funktionen auszuführen

## API Endpoints

### /api/temp_data
Auf diesem Endpoint können die Temperaturdaten als JSON abgefragt werden. 
Der Inhalt ist ein Array mit Objekten, welche jeweils die Attribute "time" und "temp" haben. 
Beides sind Numbers. Beim "time" Attribut wird die Zeit als Unix Time Stamp übergeben.
Hier ein Beispiel JSON:
```json
[
  {"time":1670940307242,"temp":21},
  {"time":1670940367242,"temp":23}
]
```

### /api/get_led
Gibt den aktuellen Status des RGB LEDs in folgender Form zurück:
```json
{"red":true,"green":true,"blue":false}
```

### /api/set_led
Setzt einen neuen Zustand für das RGB LED. Als Payload wird ein JSON in folgender Form erwartet:
```json
{"red":true,"green":true,"blue":false}
```

### /api/i2c_detect
Liefert alle aktuell angeschlossenen I2C Geräte als Array in JSON Format:
```json
[
  "0x1F",
  "0x0A"
]
```

## Client
Der Client wurde mittels [React](https://reactjs.org/) und [Material UI](https://mui.com/) umgesetzt. 
Ausserdem wurde TypeScript verwendet.
Der Startpunkt der React Applikation ist das file index.tsx unter client/src/. Hier wird die App als Komponente eingebunden.
Diese selbst ist dann in App.tsx im gleichen Ordner programmiert. In der App wird die Navbar sowie immer einer der Routes dargestellt.
Für das routing der Single Page Application wird [React-Router](https://reactrouter.com/en/main/start/overview) verwendet.
Nachfolgen ist der Route /temp als Beispiel dargestellt.
![](pictures/Client_Temparture.png)

## Phyton scripts
Für alle Funktionen der Applikation gibt es ein dazugehöriges Python-Script.
Diese werden mit pythonProcedure.sh einmal pro Sekunde ausgeführt.
I2C.py detektiert die verbunden I2C-Komponenten und speichert diese im Json-File für die Website.
RGB.py liest die gespeicherten Werte aus dem Json-File ab und setzt die RGBs nach der Konfiguration im Client.
tempsensor.py speichert neue Temperaturen, die über den I2C-Bus vom SA56004X empfangen werden, und deren Zeitstempel im zugehörigen Json-File. Falls bereits maxValue (default maxValue = 200) Elemente gespeichert sind, werden die älteste Messwerte entfernt und die neuen Werte angehängt.
IO.py ist ein noch nicht implementierter Prototyp um einzelne IOs als In-/Output zu setzen und deren Status zu manipulieren.


