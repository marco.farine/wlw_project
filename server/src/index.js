import express from 'express'
import fs from 'fs'
import * as path from 'path';

const app = express();
const port = 80;

app.use(express.json());

app.get('/api/temp_data', (req, res) => {
    const tempDataRaw = fs.readFileSync('data/tempData.json');
    const tempData = JSON.parse(tempDataRaw.toString());
    res.send(tempData);
});

app.get('/api/get_led', (req, res) => {
    const ledDataRaw = fs.readFileSync('data/ledData.json');
    const ledData = JSON.parse(ledDataRaw.toString());
    res.send(ledData);
})

app.post('/api/set_led', (req, res) => {
    fs.writeFile('data/ledData.json', JSON.stringify(req.body), () => {
    })
    res.end()
})

app.get('/api/i2c_detect', (req, res) => {
    const i2cDevicesRaw = fs.readFileSync('data/dummyI2cDevices.json');
    const i2cDevices = JSON.parse(i2cDevicesRaw.toString());
    res.send(i2cDevices);
})

app.use(express.static(path.join('..', 'client', 'build')));

// Forward any other request to index.html from there React-Router will take over.
app.use((req, res) => {
    res.sendFile(path.resolve(path.join('..', 'client', 'build', 'index.html')));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
