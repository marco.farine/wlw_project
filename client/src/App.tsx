import React from 'react';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Navbar from "./components/Navigation/Navbar";
import Temperature from "./components/screens/Temperature";
import Led from './components/screens/Led'
import I2c from "./components/screens/I2c";
import {Typography} from "@mui/material";

function App() {
    return (
        <Router>
            <Navbar/>
            <div style={{margin: 10, display: 'flex', alignItems: 'center', flexDirection: 'column', width: '100%'}}>
                <Routes>
                    <Route path="/" element={<Typography variant='h3' style={{margin: 10}}>Home</Typography>}/>
                    <Route path="/temp" element={<Temperature/>}/>
                    <Route path="/led" element={<Led/>}/>
                    <Route path="/i2c" element={<I2c/>}/>
                </Routes>
            </div>
        </Router>
    );
}

export default App;
