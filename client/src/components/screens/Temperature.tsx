import {useState, useEffect, Fragment} from "react";
import {LineChart, Line, XAxis, YAxis, ResponsiveContainer} from "recharts";
import {Typography} from "@mui/material";

function Temperature(){
    const [tempData, setTempData] = useState<{temp: number; time: string}[]>()

    useEffect(() => {
        fetch('/api/temp_data').then(resp => resp.json().then(data => {
            const dataPoints = data as {temp: number; time: number}[];
            const dataMapped = dataPoints.map(dataPoint =>  ({time: new Date(dataPoint.time).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" }), temp: dataPoint.temp}))
            setTempData(dataMapped);
        }))
    }, [])

    return (
        <Fragment>
            <Typography variant='h3'>
                Temperature
            </Typography>
            <div style={{width: "100%", display: "flex", justifyContent: "left"}}>
                <ResponsiveContainer width={"95%"} height={500}>
                    <LineChart data={tempData} height={400}>
                        <Line type={"monotone"} dataKey={"temp"} stroke={"#8884d8"}/>
                        <XAxis dataKey={"time"}/>
                        <YAxis unit={"°C"}/>
                    </LineChart>
                </ResponsiveContainer>
            </div>
        </Fragment>
    )
}

export default Temperature;