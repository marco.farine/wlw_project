import {Button, List, ListItem, Typography} from "@mui/material";
import {useState, Fragment} from "react";

function I2c(){
    const [i2cDevices, setI2cDevices] = useState<string[]>([])

    return (
        <Fragment>
            <Typography variant='h3' style={{margin: 10}}>
                I2C Tools
            </Typography>
            <Button variant='contained'
                onClick={() => {
                    fetch('/api/i2c_detect').then(resp => resp.json().then(data => {
                        setI2cDevices(data)
                    }))
                }}
            >
                I2C Detect
            </Button>
            <Typography style={{marginTop: 10}} variant="h6" component="div">
                Addresses of detected devices:
            </Typography>
            <List dense={true}>
                {i2cDevices.map(i2cDevice => (
                    <ListItem key={i2cDevice}>
                        {i2cDevice}
                    </ListItem>
                ))}
            </List>
        </Fragment>
    )
}

export default I2c;