import {useEffect, useState, Fragment} from "react";
import {Switch, FormGroup, FormControlLabel, Typography} from "@mui/material";

type Led_t = {
    red: boolean;
    green: boolean;
    blue: boolean
}

function Led() {
    const [ledData, setLedData] = useState<Led_t>()

    useEffect(() => {
        fetch('/api/get_led').then(resp => resp.json().then(data => {
            setLedData(data);
        }))
    }, [])

    return (
        <Fragment>
            <Typography variant='h3' style={{margin: 10}}>
                RGB LED Control
            </Typography>
            <FormGroup style={{margin: '10px 20px'}}>
                {ledData ? Object.keys(ledData).map(color => (
                    <FormControlLabel
                        key={color}
                        control={
                            <Switch
                                checked={ledData[color as keyof typeof ledData]}
                                onChange={() => {
                                    const newData : Led_t = Object.assign({}, ledData);
                                    newData[color as keyof typeof newData] =  !ledData[color as keyof typeof ledData];
                                    setLedData(newData);
                                    fetch('/api/set_led', {
                                        method: 'POST',
                                        headers: {
                                            'Content-Type': 'application/json'
                                        },
                                        body: JSON.stringify(newData)
                                    })
                                }}
                            />
                        }
                        label={color.charAt(0).toUpperCase() + color.slice(1)}
                    />
                )) : null}
            </FormGroup>
        </Fragment>
    );
}

export default Led;
