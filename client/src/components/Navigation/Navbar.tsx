import React from "react";
import {
    AppBar,
    Toolbar,
    CssBaseline,
    Typography,
} from "@mui/material";
import {makeStyles} from "tss-react/mui"
import { Link } from "react-router-dom";

const useStyles = makeStyles()((theme) => ({
    navlinks: {
        marginLeft: theme.spacing(10),
        display: "flex",
    },
    title: {
        flexGrow: "1",
        textDecoration: "none",
        color: "white",
    },
    link: {
        textDecoration: "none",
        color: "white",
        fontSize: "20px",
        marginLeft: theme.spacing(3),
        "&:hover": {
            color: "yellow",
            borderBottom: "1px solid white",
        },
    },
}));

function Navbar() {
    const {classes} = useStyles();

    return (
        <AppBar position="static">
            <CssBaseline />
            <Toolbar>
                <Typography variant="h5" className={classes.title}>
                    <Link to="/" className={classes.title}>
                        Raspberry
                    </Link>
                </Typography>

                <div className={classes.navlinks}>
                    <Link to="/" className={classes.link}>
                        Home
                    </Link>
                    <Link to="/temp" className={classes.link}>
                        Temperature
                    </Link>
                    <Link to="/led" className={classes.link}>
                        Led
                    </Link>
                    <Link to="/i2c" className={classes.link}>
                        I2C
                    </Link>
                </div>
            </Toolbar>
        </AppBar>
    );
}
export default Navbar;